# BMI

Body Mass Index is a simple calculation using a person’s height and weight. The
formula is BMI = kg/m2 where kg is a person’s weight in kilograms and m2 is their height in meters squared. A BMI of 25.0 or more is overweight, while the healthy range is 18.5 to 24.9.


# Pipelines

- Build
- Test
	- SAST (using gitlab security Templates)
- Deploy
- Post Deploy
	- DAST (not running because it's need Gitlab Ultimate licence)

## How to use

Example:

Input:
https://sayfuddin.xyz/?height=70&weight=167

Output:
{
	“bmi”: 25.1,
	“label”: “overweight”
}

## Additional

- Cloudflare Free Plan
