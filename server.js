'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
	let result ={}
	let pesan =""
	if ((!req.query.height)&& (!req.query.weight)){
		pesan ="Body Mass Index is a simple calculation using a person’s height and weight. The formula is BMI = kg/m2 where kg is a person’s weight in kilograms and m2 is their height in meters squared. A BMI of 25.0 or more is overweight, while the healthy range is 18.5 to 24.9." 
		result = {
			Message: pesan,
			Usage :"Use GET method with parameter Height (in kg) & Weight (in cm)"
		};
	} 

	else if((!checkvalueparameter(req.query.height)) || (!checkvalueparameter(req.query.weight)))  {
		pesan ="Invalid Parameter value" 
		result = {
			Message: pesan,
			Data : {"Parameter":{height:req.query.height,weight:req.query.weight}}

		};
	}
	else if(req.query.height <=0 || req.query.weight <=0){
		pesan ="Parameter value must be greater than 0" 
		result = {
			Message: pesan,
			Data : {"Parameter":{height:req.query.height,weight:req.query.weight}}

		};
	}
	else{
	let height = parseFloat(req.query.height)  // true
    let weight = parseFloat(req.query.weight)// true
    pesan = bmicalculation(height,weight) 
    result =pesan;

}

res.send(result)


})

function bmicalculation(height,weight) {
	let bmi = Math.round(weight / ((height/100)**2).toFixed(2))
	if (bmi < 18.5){
		return {
			"bmi": bmi,
			"label": "Underweight",
		}
	}
	else if (bmi >= 18.5 && bmi < 24.9){
		return {
			"bmi": bmi,
			"label": "Normal weight",
		}
	}
	else if (bmi >= 25.0 && bmi < 29.9){
		return {
			"bmi": bmi,
			"label": "Overweight",
		}
	}
	else if (bmi >= 30.0 && bmi < 39.9)
	{
		return {
			"bmi": bmi,
			"label": "Obese",
		}
	}
	else (bmi >= 40.0)
	{
		return {
			"bmi": bmi,
			"label": "Morbidly Obese",
		}

	}
}
function checkvalueparameter(value) {

	return (/^-?\d+$/.test(value) || /^\d+\.\d+$/.test(value));
}



app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);